<?php
// do `composer dump_autoload` after changing "autoload" in config.json
require __DIR__ . '/vendor/autoload.php';

use Gloomy\Model;
use Gloomy\Helpers;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

/* config example (all parameters become variables):

; FS settings
temp_folder = ./temp

; vk settings
vk_token = xxx
vk_group_id = xxx
vk_app_id = xxx

; tumbler access key
tumblr_consumer_key = xxx
tumblr_consumer_secret = xxx
tumblr_blog_address = xxx

; MySQL settings
sql_user = xxx
sql_pass = xxx
sql_base = xxx
sql_table = xxx

; interval in seconds
interval = 3553
*/

try {
    $config = Helpers\FileSystem::loadConfig('config.ini');
} catch (\Exception $e) {
    die($e->getMessage() . PHP_EOL);
}

extract($config);

// init objects
$tbClient = new Tumblr\API\Client($tumblr_consumer_key, $tumblr_consumer_secret);
$sql = new Model\Sql($sql_user, $sql_pass, $sql_base);
$vk = new Helpers\VkHelper($vk_token, $vk_group_id, $temp_folder);

// get data
$photoPosts = $tbClient->getBlogPosts($tumblr_blog_address, ['type' => 'photo']);
$post_ids = $sql->prepCheckDuplicateIds(Helpers\TumblrHelper::getIds($photoPosts), $sql_table);
$posts = Helpers\TumblrHelper::format($photoPosts, $post_ids);

if (count($posts) > 0) {
    print 'New posts: ' . count($posts) . PHP_EOL;
    $now = time();
    $d = 200;
    foreach ($posts as $post) {
        $time = $now + $d;
        if (isset($post['photos'])) {
            try {
                $vk->postWallPhoto($post['tags'], $post['photos'], $time, $post['link']);
            } catch (\Exception $e) {
                die($e->getMessage() . PHP_EOL);
            }
        }
        $d += (int)$interval;
    }
    // add new posts to base
    if ($sql->addToTable($posts, $sql_table)) {
        print 'Successfully added new posts!' . PHP_EOL;
    }

    // then delete temp folder
    try {
        Helpers\FileSystem::deleteDir($temp_folder);
    } catch (\Exception $e) {
        die($e->getMessage() . PHP_EOL);
    }
} else {
    print 'No new posts! Come later...' . PHP_EOL;
}