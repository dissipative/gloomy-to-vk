<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 18.12.2016
 * Time: 20:41
 */

namespace Gloomy\Helpers;

use Gloomy\Model;

class FileSystem
{
    static $configFields = [
        'temp_folder',
        'vk_token',
        'vk_group_id',
        'vk_app_id',
        'tumblr_consumer_key',
        'tumblr_consumer_secret',
        'tumblr_blog_address',
        'sql_user',
        'sql_pass',
        'sql_base',
        'sql_table',
        'interval'
    ];

    public static function loadConfig($configName)
    {
        if (file_exists($configName)) {
            $config = parse_ini_file($configName);
            foreach (static::$configFields as $parameter) {
                if (!array_key_exists($parameter, $config) || !$config[$parameter])
                    throw new \Exception("$parameter is not set in $configName");
            }
            return $config;

        } else {
            throw new \Exception("$configName does not exist");
        }
    }

    public static function deleteDir($dirPath)
    {

        if (!is_dir($dirPath)) {
            throw new \Exception("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                static::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

}