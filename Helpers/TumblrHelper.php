<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 15.12.2016
 * Time: 17:06
 */

namespace Gloomy\Helpers;

use Gloomy\Model;

class TumblrHelper extends Model\WebIO
{
    public static function getIds($posts)
    {
        $ids = [];
        foreach ($posts->posts as $post) {
            array_push($ids, $post->id);
        }
        return $ids;
    }

    public static function format($posts, $ids)
    {
        $result = [];
        foreach ($posts->posts as $key => $post) {
            $type = $post->type;
            if (in_array($post->id, $ids)) {
                $result[$key]['post_id'] = $post->id;
                if (!empty($post->tags)) {
                    $tags = '#' . implode(',#', $post->tags);
                    $tags = str_replace(' ', '_', $tags);
                    $tags = str_replace(',', ', ', $tags);
                } else {
                    $tags = '';
                }
                $result[$key]['tags'] = $tags;
                if ($type == 'photo') {
                    $result[$key]['link'] = $post->post_url;
                    foreach ($post->photos as $photo) {
                        $result[$key]['photos'][] = $photo->original_size->url;
                    }
                } elseif ($type == 'audio') {
                    $result[$key]['link'] = $post->source_url;
                }
            }
        }
        return $result;
    }

}
