<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 15.12.2016
 * Time: 17:05
 */

namespace Gloomy\Helpers;

use Gloomy\Model;

class VkHelper extends Model\WebIO
{
    private $tempFolder, $token, $group_id;

    public function __construct($token, $group_id, $tempFolder)
    {
        $this->token = $token;
        $this->group_id = $group_id;
        $this->tempFolder = $tempFolder;
    }

    public function apiAction($action, $params = false, $no_group = false)
    {
        $base_url = "https://api.vk.com/method/$action";
        $base_params = $no_group ? http_build_query(['access_token' => $this->token,]) :
            http_build_query(['group_id' => $this->group_id,
                'access_token' => $this->token,]);
        $target_url = "$base_url?$base_params";

        if (is_array($params)) {
            $params = http_build_query($params);
            $target_url = "$target_url&$params";
        }
        $response = $this->useCurl($target_url);
        return $response;
    }

    public function uploadFileToServer($server, $filename, $type = 'photo')
    {
        $upload_url = $server->response->upload_url;
        if (preg_match('/^http/', $filename)) {
            $filename = $this->getTemp($filename, $this->tempFolder);
        }

        $curlFN = $this->getCurlValue($filename);
        $response = $this->useCurl($upload_url, 'post', array($type => $curlFN));
        return get_object_vars($response);
    }

    public function postWallPhoto($message, $filenames, $date, $link)
    {
        $photoServer = $this->apiAction('photos.getWallUploadServer'); //firstly get server to upload
        $docsServer = $this->apiAction('docs.getUploadServer', false, true);
        if ($photoServer->response && $docsServer->response) {
            $photoIds = [];
            foreach ($filenames as $filename) {
                // then upload the photo or gif
                $fileInfo = pathinfo($filename);

                if ($fileInfo['extension'] == 'gif') {
                    $upInfo = $this->uploadFileToServer($docsServer, $filename, 'file');
                    $photo = $this->apiAction('docs.save', [
                        'file' => $upInfo['file'],
                        'title' => $fileInfo['filename'],
                    ]);
                    if ($photo->response)
                        $photoIds[] = 'doc' . $photo->response[0]->owner_id . '_' . $photo->response[0]->did;
                } else {
                    $upInfo = $this->uploadFileToServer($photoServer, $filename);
                    $photo = $this->apiAction('photos.saveWallPhoto', [
                        'photo' => $upInfo['photo'],
                        'hash' => $upInfo['hash'],
                        'server' => $upInfo['server'],
                    ]);
                    if ($photo->response)
                    $photoIds[] = $photo->response[0]->id;
                }

            }

            if (!empty($photoIds)) {
                $photoIds = implode(',', $photoIds);
                // then save photo for wall
                $post = $this->apiAction('wall.post', [
                    'owner_id' => -$this->group_id,
                    'from_group' => '1',
                    'message' => $message,
                    'publish_date' => $date ? $date : time(),
                    'attachments' => $photoIds . ',' . $link,
                ]);
            } else {
                $post = false;
            }

            return $post;
        } else {

            $error = $photoServer->error ?
                $photoServer->error->error_msg : $docsServer->error->error_msg;

            throw new \Exception('Error in server response! ' . $error);
            return false;
        }
    }

    public function postWallLink($link, $date)
    {
        $post = $this->apiAction('wall.post', array(
            'owner_id' => -$this->group_id,
            'from_group' => '1',
            'attachments' => $link,
            'publish_date' => $date ? $date : time(),
        ));
        return $post;
    }

}
