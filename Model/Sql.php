<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 15.12.2016
 * Time: 17:06
 */

namespace Gloomy\Model;

class Sql extends WebIO
{

    public function __construct($sqluser, $sqlpass, $database)
    {
        $this->conn = new \PDO("mysql:host=localhost;dbname=$database", $sqluser, $sqlpass);
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function __destruct()
    {
        $this->conn = null;
    }

    private function tableExists($table)
    {
        try {
            $result = $this->conn->query("SELECT 1 FROM $table LIMIT 1");
        } catch (\PDOException $e) {
            return false;
        }
        return $result;
    }

    public function addToTable($array, $table)
    {
        try {
            $conn = $this->conn;
            $conn->beginTransaction();
            foreach ($array as $value) {
                $sql = $conn->prepare("INSERT INTO $table (link, post_id)
					VALUES (:link, :post_id)");
                $sql->execute(array(
                        'link'    => $value['link'],
                        'post_id' => $value['post_id'],
                    )
                );
            }
            $conn->commit();
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
        $conn = null;
        return true;
    }

    public function prepCheckDuplicateIds($array, $table)
    {
        if (!is_array($array)) {
            return 'Input must be in array';
        } else {
            try {
                $conn = $this->conn;
                if (!$this->tableExists($table)) {
                    $sql = "CREATE TABLE $table (
					id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					link VARCHAR(150) NOT NULL,
					post_id VARCHAR(20) NOT NULL,
					upd_date TIMESTAMP
					)";
                    $conn->exec($sql);
                } else {
                    $sql = $conn->prepare("SELECT post_id FROM $table");
                    $sql->execute();
                    $result = $sql->fetchAll(\PDO::FETCH_ASSOC);

                    foreach ($array as $key => &$post) {
                        foreach ($result as $baseVal) {
                            if ($baseVal['post_id'] == $post) {
                                print 'Already in base: ' . $post . PHP_EOL;
                                unset($array[$key]);
                                break;
                            }
                        }
                    }

                }
            } catch (\PDOException $e) {
                return $e->getMessage();
            }
            $conn = null;
            return $array;
        }
    }

}
