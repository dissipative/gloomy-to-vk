<?php
namespace Gloomy\Model;

abstract class WebIO
{

    public static function useCurl($url, $method = 'get', $fields = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($method == 'post') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
        }
        if ($method == 'download') {
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = ($method == 'download' ? curl_exec($ch) : json_decode(curl_exec($ch)));
        curl_close($ch);
        if (!$response) {
            return 'error of cURL!';
        } else {
            return $response;
        }

    }

    public function getCurlValue($filename)
    {
        if (function_exists('curl_file_create')) {
            return curl_file_create($filename, '', '');
        } else {
            $value = "@{$filename}";
            return $value;
        }
    }

    public function getTemp($linkToFile, $tempFolder)
    {
        $file = $this->useCurl($linkToFile, 'download');

        $ext = pathinfo($linkToFile)['extension'];
        $ext = $ext? ".$ext" : '.jpg';

        $dirName = $tempFolder . '/' . date('Y_m_d');
        if (!file_exists($dirName)) {
            mkdir($dirName, 0777, true);
        }
        $fileName = $dirName . '/' . md5(mt_rand(0, 365) . uniqid()) . $ext;
        $fp = fopen($fileName, 'w');
        fwrite($fp, $file);
        fclose($fp);
        return $fileName;
    }

}