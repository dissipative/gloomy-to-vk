<?php

if (isset($_REQUEST['expires_in'])) {

    echo 'Your access token: ' . $_REQUEST['access_token'];

} else {

    $config = parse_ini_file('config.ini');

    if (!empty($config)) {
        echo 'Redirecting to vk...';
        $client_id     = $config['vk_app_id'];
        $display       = 'page';
        $redirect_uri  = 'https://oauth.vk.com/blank.html';
        $scope         = 'photos,wall,groups,offline,docs';
        $response_type = 'token';
        $v             = '5.21';

        header("Location: https://oauth.vk.com/authorize?client_id=$client_id&display=$display&redirect_uri=$redirect_uri&scope=$scope&response_type=token&v=$v");
    }

}
